#pragma once

#ifndef  XVIEWPORT_H
#define XVIEWPORT_H

#include "Header.h"

HRESULT createViewport(LPDIRECT3DDEVICE9 * gDevice, int width, int height);
void setProjectionMatrix(LPDIRECT3DDEVICE9 * gDevice, int width, int height);

#endif // ! XVIEWPORT_H
