#pragma once
#ifndef XLIGHT_H
#define XLIGHT_H

#include "Header.h"
class XSpotLight
{
	public:
		XSpotLight() {}

		LPDIRECT3DDEVICE9* p_gDevice;

		void setD3DDevice(LPDIRECT3DDEVICE9 * gDevice) { p_gDevice = gDevice; }
		HRESULT initSpotLight();// (LPDIRECT3DDEVICE9 * gDevice);
		D3DLIGHT9 light;

	private:
};


class XPointLight
{
	public:
	XPointLight() {}

	LPDIRECT3DDEVICE9 * p_gDevice;

	void setD3DDevice(LPDIRECT3DDEVICE9 * gDevice) { p_gDevice = gDevice; }
	HRESULT initPointLight();
	D3DLIGHT9 light;

	private:

};
#endif