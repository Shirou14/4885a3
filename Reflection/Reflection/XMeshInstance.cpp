//this class is designed to render the mesh in reference to a XMesh Object
#include "Header.h"
#include "XMeshInstance.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Default constructor
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
XMeshInstance::XMeshInstance()
{
	m_pMesh = NULL;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Release
Summary:
Release resouces
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void XMeshInstance::Release()

{
	// Mesh data is Released in CMesh 
	m_pMesh = NULL;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Set the CMesh reference
Parameters:
[in] pMesh � Pointer to a CMesh
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void XMeshInstance::SetMesh(XMesh* pMesh)

{
	Release();
	m_pMesh = pMesh;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Renders the mesh
Parameters:
[in] pDevice � D3D Device
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void XMeshInstance::Render(LPDIRECT3DDEVICE9 pDevice)
{
	if (pDevice && m_pMesh)
	{
		pDevice->SetTransform(D3DTS_WORLD, GetTransform());
		DWORD numMaterials = m_pMesh->GetNumMaterials();
		for (DWORD i = 0; i < numMaterials; i++)
		{
			pDevice->SetMaterial(m_pMesh->GetMeshMaterial(i));
			pDevice->SetTexture(0, m_pMesh->GetMeshTexture(i));
			m_pMesh->GetMesh()->DrawSubset(i);
		}

	}

}