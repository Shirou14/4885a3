#pragma once
#pragma once
#ifndef XCAMERA_H
#define XCAMERA_H
#include "Header.h"

class XCamera
{
	public:
	enum CameraType { LANDOBJECT, AIRCRAFT };
	XCamera();
	XCamera(CameraType cameraType) :_cameraType(cameraType) {}
	~XCamera() {}
	void strafe(float units); // left/right
	void fly(float units); // up/down
	void walk(float units); // forward/backward
	void pitch(float angle); // rotate on right vector
	void yaw(float angle); // rotate on up vector
	void roll(float angle); // rotate on look vector
	void setCameraType(CameraType cameraType);
	void getPosition(D3DXVECTOR3* pos);
	void setPosition(D3DXVECTOR3* pos);
	void getRight(D3DXVECTOR3* right);
	void getUp(D3DXVECTOR3* up);
	void getLook(D3DXVECTOR3* look);
	void getViewMatrix(D3DXMATRIX* V);
	private:
	CameraType _cameraType;
	D3DXVECTOR3 _right;
	D3DXVECTOR3 _up;
	D3DXVECTOR3 _look;
	D3DXVECTOR3 _pos;
};

#endif