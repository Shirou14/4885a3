//this class is designed to load the mesh information and store it for use
//follows the http://www.chadvernon.com/blog/resources/directx9/loading-a-static-mesh-x-format/ tutorial

#include "Header.h"
#include "XMesh.h"

//Default constructor. Initiates all members as null or zero
XMesh::XMesh() :m_pMesh(NULL),
m_numMaterials(0),
m_pMeshMaterials(NULL),
m_ppMeshTextures(NULL)
{}

//Cleans up resources
void XMesh::Release()

{
	// Delete the materials 
	m_pMeshMaterials = NULL;
	// Delete the textures 
	if (m_ppMeshTextures)
	{

		for (DWORD i = 0; i < m_numMaterials; i++)
		{
			//SAFE_RELEASE(m_ppMeshTextures[i]);
			if (m_ppMeshTextures[i])
			{
				m_ppMeshTextures[i]->Release();
				m_ppMeshTextures[i] = NULL;
			}
		}
	}

	m_ppMeshTextures = NULL;

	if (m_pMesh != NULL)
	{
		m_pMesh->Release();
		m_pMesh = NULL;
	}
}

/*
We load an X file mesh with no animation.

pDevice is our D3D Device
file is the file name of the mesh
Returns TRUE if load was successful, FALSE otherwise
*/
BOOL XMesh::Load(HWND hwnd, LPDIRECT3DDEVICE9 pDevice, LPCWSTR file)

{
	Release();//release any previous data
	LPD3DXBUFFER pMaterialBuffer;
	char path[MAX_PATH];

	HRESULT hr = D3DXLoadMeshFromX(file, D3DXMESH_MANAGED, pDevice, NULL, &pMaterialBuffer, NULL, &m_numMaterials, &m_pMesh);
	if (FAILED(hr))
	{
		//SHOWERROR(�D3DXLoadMeshFromX � Failed�, __FILE__, __LINE__);
		MessageBox(0, TEXT("Failed to load mesh from location."), TEXT("Error"), 0);
		return FALSE;
	}

	// Store material and texture information 
	D3DXMATERIAL* pMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();

	m_pMeshMaterials = new D3DMATERIAL9[m_numMaterials];
	m_ppMeshTextures = new LPDIRECT3DTEXTURE9[m_numMaterials];

	// Copy the materials and textures from the buffer to the member arrays 
	for (DWORD i = 0; i < m_numMaterials; i++)
	{
		m_pMeshMaterials[i] = pMaterials[i].MatD3D;
		m_pMeshMaterials[i].Ambient = m_pMeshMaterials[i].Diffuse;
		// Create the texture if it exists 
		m_ppMeshTextures[i] = NULL;
		if (pMaterials[i].pTextureFilename)
		{

			char texture[MAX_PATH] = { 0 };
			wchar_t wTexture[MAX_PATH];
			//CUtility::GetMediaFile(pMaterials[i].pTextureFilename, texture);
			strcpy_s(texture, "..//");
			strcat_s(texture, pMaterials[i].pTextureFilename);
			size_t* x = 0;
			mbstowcs_s(x, wTexture, (size_t)MAX_PATH, texture, _TRUNCATE);
			//wchar_t temp[strlen()] = L"//..//";

			hr = D3DXCreateTextureFromFile(pDevice, wTexture, &m_ppMeshTextures[i]);
			if (FAILED(hr))
			{
				//SHOWERROR(�Failed to load mesh texture�, __FILE__, __LINE__);
				MessageBox(0, TEXT("Failed to Create texture from file"), TEXT("Error"), 0);
				return FALSE;
			}
		}
	}

	// Don�t need this no more! 
	pMaterialBuffer->Release();

	return TRUE;

}