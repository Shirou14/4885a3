#pragma once

#ifndef GLOBAL_H
#define GLOBAL_H

#include "Header.h"
#include "XmeshInstance.h"
#include "XLight.h"
#include "XCamera.h"

class Global
{
	public:
		//////////////////////////////
		// Variables               //
		////////////////////////////

		HWND g_hwndGlobal;//handle to the main window

		LPDIRECT3D9 g_pD3D = 0;//com object
		LPDIRECT3DDEVICE9 g_pDevice;//graphics device
		LPDIRECT3DSURFACE9 mainSurface;
		D3DXMATRIX view;

		int RESULT;
		int g_deviceWidth;
		int g_deviceHeight;
		int select = 0;

		LARGE_INTEGER startTime = { 0 };
		LARGE_INTEGER endTime = { 0 };
		LARGE_INTEGER freq = { 0 };

		wchar_t* MESH_ONE = TEXT("..//Low_Poly_Plant_.x");
		wchar_t* MESH_TWO = TEXT("..//Fan_Done2_Rigged.blend.x");

		XMesh m1;
		XMesh m2;
		XMeshInstance mi1;
		XMeshInstance mi2;

		XSpotLight sp1;
		XPointLight pl1;

		XCamera viewCamera;

		double timeDelta = 0.0;

		//////////////////////////////////////////
		// Method Definitions                  //
		////////////////////////////////////////
		int InitDirect3DDevice(HWND hWndTarget, int Width, int Height, BOOL bWindowed, D3DFORMAT FullScreenFormat, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9* ppDevice)
		{
			D3DPRESENT_PARAMETERS d3dpp;//rendering information
			D3DDISPLAYMODE d3ddm;//current display mode info
			HRESULT r = 0;

			if (*ppDevice)//if the ppDevice exists, fix that
				(*ppDevice)->Release();

			ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
			r = pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);
			if (FAILED(r))
			{
				return E_FAIL;
			}

			d3dpp.BackBufferWidth = Width;
			d3dpp.BackBufferHeight = Height;
			d3dpp.BackBufferFormat = bWindowed ? d3ddm.Format : FullScreenFormat;
			d3dpp.BackBufferCount = 1;
			d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
			d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD; //D3DSWAPEFFECT_COPY
			d3dpp.hDeviceWindow = hWndTarget;
			d3dpp.Windowed = bWindowed;
			d3dpp.EnableAutoDepthStencil = TRUE;
			d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
			d3dpp.FullScreen_RefreshRateInHz = 0;
			d3dpp.PresentationInterval = bWindowed ? 0 : D3DPRESENT_INTERVAL_IMMEDIATE;
			d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

			r = pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWndTarget, D3DCREATE_SOFTWARE_VERTEXPROCESSING,
				&d3dpp, ppDevice);
			if (FAILED(r))
			{
				return E_FAIL;
			}

			g_deviceHeight = Height;
			g_deviceWidth = Width;
		}
};

#endif