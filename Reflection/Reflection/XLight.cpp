#include "XLight.h"

HRESULT XSpotLight::initSpotLight()
{
	HRESULT r;
	D3DMATERIAL9 mat;

	if (!*p_gDevice)
		return E_FAIL;

	ZeroMemory(&light, sizeof(light));

	light.Type = D3DLIGHT_SPOT;
	light.Diffuse = D3DXCOLOR(0x00FF1111);
	light.Position = D3DXVECTOR3(-1.0f, 5.0f, 0.0f);
	light.Direction = D3DXVECTOR3(0.0f, -1.0f, 0.0f);
	light.Range = 100.0f;    // a range of 100
	light.Attenuation0 = 0.0f;    // no constant inverse attenuation
	light.Attenuation1 = 0.125f;    // only .125 inverse attenuation
	light.Attenuation2 = 0.0f;    // no square inverse attenuation
	light.Phi = D3DXToRadian(40.0f);    // set the outer cone to 30 degrees
	light.Theta = D3DXToRadian(15.0f);    // set the inner cone to 10 degrees
	light.Falloff = 1.0f;    // use the typical falloff

	(*p_gDevice)->SetLight(1, &light);
	(*p_gDevice)->LightEnable(1, TRUE);

	ZeroMemory(&mat, sizeof(D3DMATERIAL9));
	mat.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mat.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	(*p_gDevice)->SetMaterial(&mat);


	return S_OK;
}

HRESULT XPointLight::initPointLight()
{
	HRESULT r;
	D3DMATERIAL9 mat;

	if (!*p_gDevice)
		return E_FAIL;

	ZeroMemory(&light, sizeof(light));

	light.Type = D3DLIGHT_POINT;
	light.Diffuse = D3DXCOLOR(0.5f, 0.5f, 1.0f, 1.0f);
	light.Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	light.Range = 100.0f;    // a range of 100
	light.Attenuation0 = 0.0f;    // no constant inverse attenuation
	light.Attenuation1 = 0.125f;    // only .125 inverse attenuation
	light.Attenuation2 = 0.0f;    // no square inverse attenuation
	light.Falloff = 1.0f;    // use the typical falloff

	(*p_gDevice)->SetLight(0, &light);
	//(*p_gDevice)->Light
	(*p_gDevice)->LightEnable(0, TRUE);

	ZeroMemory(&mat, sizeof(D3DMATERIAL9));
	mat.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mat.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	(*p_gDevice)->SetMaterial(&mat);


	return S_OK;
}