#include "XViewport.h"

HRESULT createViewport(LPDIRECT3DDEVICE9 * gDevice, int width, int height)
{
	HRESULT r = 0;

	if (!gDevice)
	{
		return E_FAIL;
	}

	D3DVIEWPORT9 Viewport;

	Viewport.X = 0;
	Viewport.Y = 0;
	Viewport.Width = width;
	Viewport.Height = height;
	Viewport.MinZ = 0.0f;
	Viewport.MaxZ = 1.0f;

	r = (*gDevice)->SetViewport(&Viewport);

	return r;

}

void setProjectionMatrix(LPDIRECT3DDEVICE9 * gDevice, int width, int height)
{
	D3DXMATRIX ProjectionMatrix;
	ZeroMemory(&ProjectionMatrix, sizeof(D3DXMATRIX));

	float ScreenAspect = (float)width / (float)height;
	float FOV = D3DX_PI / 4;

	D3DXMatrixPerspectiveFovLH(&ProjectionMatrix, FOV, ScreenAspect, 1.0f, 1000.0f);

	(*gDevice)->SetTransform(D3DTS_PROJECTION, &ProjectionMatrix);
}