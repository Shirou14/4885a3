//#pragma once
#include "Header.h"
#include "DXWorldTransform.h" 

class XMesh
{
	public:
	XMesh();
	~XMesh() { Release(); }
	BOOL Load(HWND hwnd, LPDIRECT3DDEVICE9 pDevice, LPCWSTR file);
	void Release();

	LPD3DXMESH GetMesh() { return m_pMesh; }
	DWORD GetNumMaterials() { return m_numMaterials; }
	D3DMATERIAL9* GetMeshMaterial(int i) { return &m_pMeshMaterials[i]; }
	LPDIRECT3DTEXTURE9 GetMeshTexture(int i) { return m_ppMeshTextures[i]; }

	private:
	LPD3DXMESH m_pMesh;
	DWORD m_numMaterials;
	D3DMATERIAL9 *m_pMeshMaterials;
	LPDIRECT3DTEXTURE9 *m_ppMeshTextures;
};
