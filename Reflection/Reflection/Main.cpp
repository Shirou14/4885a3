#include "Global.h"
#include "XViewport.h"

Global g;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pstrCmdLine, int iCmdShow)
{
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wcx = { 0 };

	static WCHAR appName[] = TEXT("Jay's Super Special Awesome Direct X Assignment deux");

	wcx.cbSize = sizeof(WNDCLASSEX);
	wcx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcx.cbClsExtra = 0;
	wcx.cbWndExtra = 0;
	wcx.lpfnWndProc = WndProc;
	wcx.hInstance = hInstance;
	wcx.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	wcx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wcx.hIconSm = LoadIcon(NULL, IDI_HAND);
	wcx.hCursor = LoadCursor(NULL, IDC_CROSS);
	wcx.lpszMenuName = NULL;
	wcx.lpszClassName = appName;
	//wcx.hCursor = 

	RegisterClassEx(&wcx);

	hwnd = CreateWindowEx
	(
		NULL,
		appName,
		appName,
		WS_EX_TOPMOST | WS_POPUP,
		0,
		0,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	g.g_hwndGlobal = hwnd;

	//We're going to try and load in some mesh's here to be rendered when the window updates.

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	if (FAILED(GameInit(hwnd)))
	{
		MessageBox(hwnd, TEXT("Initialization has failed"), NULL, NULL);
	}

	while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) //we peek at the message to see if it is relevant to us. PM_REMOVE removes it from the queue.
		{
			if (msg.message == WM_QUIT)
				break;
			//translates and sends the message to wndProc
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			GameLoop(hwnd);
		}
		//GameLoop(hwnd);
		//Render_Frame();
	}
	GameShutdown();//clean up the game here
	return msg.wParam;
}

//running the window
long CALLBACK WndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
		case WM_CREATE:
		{
			return 0;
		}
		case WM_PAINT:
		{
			ValidateRect(hWnd, NULL);
			return 0;
		}
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		default:
		{
			return DefWindowProc(hWnd, uMessage, wParam, lParam);
		}
	}
}


int Render_Frame(HWND hwnd)//We do all the calculactions and functions necessary to do every frame
{
	HRESULT r;
	D3DLOCKED_RECT plockedRect;//locked area of display memory (buffer in actuality)
	LPDIRECT3DSURFACE9 pBackSurf = 0;

	//clear window to a deep blue (actually pink(and by pink I mean white, but whatevs) but whatevs)
	g.g_pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(255, 255, 255), 1.0f, 0);

	//we get the backbuffer and set it to pBackSurf
	r = g.g_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackSurf);

	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not get Back Buffer"), NULL, NULL);
		return E_FAIL;
	}

	g.g_pDevice->BeginScene(); //begin the 3D scene

	g.mi1.RotateRel(0, 0.01, 0);
	g.mi2.RotateRel(0, 0.01, 0);

	g.mi1.Render(g.g_pDevice);
	g.mi2.Render(g.g_pDevice);

	g.g_pDevice->EndScene(); //end the 3D scene

	g.g_pDevice->Present(NULL, NULL, NULL, NULL);//present the surface
	return S_OK;
}


int GameInit(HWND hwnd)
{
	HRESULT r = 0;
	HBITMAP hBitmap;
	BITMAP bitmap;
	g.mainSurface = 0;

	g.g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (g.g_pD3D == NULL)
	{
		MessageBox(hwnd, TEXT("Could not create D3D object. \nSorry"), NULL, NULL);
		return E_FAIL;
	}
	//We create a parameters object
	D3DPRESENT_PARAMETERS d3dpp;

	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;//make it a window
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD; //Discards the old frame. no need to keep.
	d3dpp.hDeviceWindow = hwnd;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferWidth = SCREEN_WIDTH;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;

	r = g.InitDirect3DDevice(hwnd, 640, 480, TRUE, D3DFMT_X8R8G8B8, g.g_pD3D, &g.g_pDevice);
	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not initialize D3D object. \nSorry"), NULL, NULL);
		return E_FAIL;
	}

	//r = LoadBitmapToSurface("Otter oh my gawd!.bmp",)

	r = g.g_pDevice->CreateOffscreenPlainSurface(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), D3DFMT_X8R8G8B8, D3DPOOL_SYSTEMMEM, &g.mainSurface, NULL);
	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not Create off screen plain surface. \nSorry"), NULL, NULL);
		return E_FAIL;
	}

	//r = LoadBitmapToSurface()

	r = createViewport(&g.g_pDevice, g.g_deviceWidth, g.g_deviceHeight);
	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not Create Viewport. \nSorry"), NULL, NULL);
		return E_FAIL;
	}

	setProjectionMatrix(&g.g_pDevice, g.g_deviceWidth, g.g_deviceHeight);

	D3DXMatrixLookAtLH(&g.view, &D3DXVECTOR3(0.0f, 3.0f, -5.0f),
		&D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		&D3DXVECTOR3(0.0f, 1.0f, 0.0f));

	g.g_pDevice->SetTransform(D3DTS_VIEW, &g.view);
	g.g_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//g.g_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_);
	//g.g_pDevice->SetRenderState(D3DRS_AMBIENT, 0x00888888);
	g.g_pDevice->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);

	//set the first mesh and create it
	if (!(g.m1.Load(hwnd, g.g_pDevice, g.MESH_ONE)))
	{
		MessageBox(hwnd, TEXT("Could not load MESH_ONE. \nSorry"), NULL, NULL);
		return E_FAIL;
	}
	else
	{
		g.mi1.SetMesh(&g.m1);
		g.mi1.TranslateAbs(1, 0, 0);
	}

	//set up and create the second mesh
	if (!(g.m2.Load(hwnd, g.g_pDevice, g.MESH_TWO)))
	{
		MessageBox(hwnd, TEXT("Could not load MESH_TWO. \nSorry"), NULL, NULL);
		return E_FAIL;
	}
	else
	{
		g.mi2.SetMesh(&g.m2);
		g.mi2.TranslateAbs(-1, 0, 0);
	}

	//now we'll set up the light
	g.sp1.setD3DDevice(&g.g_pDevice);
	g.sp1.initSpotLight();
	g.pl1.setD3DDevice(&g.g_pDevice);
	g.pl1.initPointLight();

	return S_OK;
}

//this loop runs every possible time when we're not peaking at something
int GameLoop(HWND hwnd)
{
	//increase fps
	//counter.increment();

	QueryPerformanceCounter(&g.startTime);

	//here we;re theoretically moving the camera
	if (g.select == 0)//controlling the camera
	{
		if (::GetAsyncKeyState('W') & 0x8000f)
			g.viewCamera.walk(0.04f);
		if (::GetAsyncKeyState('S') & 0x8000f)
			g.viewCamera.walk(-0.04f);
		if (::GetAsyncKeyState('A') & 0x8000f)
			g.viewCamera.strafe(-0.04f);
		if (::GetAsyncKeyState('D') & 0x8000f)
			g.viewCamera.strafe(0.04f);
		if (::GetAsyncKeyState('R') & 0x8000f)
			g.viewCamera.fly(0.04f);
		if (::GetAsyncKeyState('F') & 0x8000f)
			g.viewCamera.fly(-0.04f);
		if (::GetAsyncKeyState(VK_UP) & 0x8000f)
			g.viewCamera.pitch(0.01f);
		if (::GetAsyncKeyState(VK_DOWN) & 0x8000f)
			g.viewCamera.pitch(-0.01);
		if (::GetAsyncKeyState(VK_LEFT) & 0x8000f)
			g.viewCamera.yaw(-0.01f);
		if (::GetAsyncKeyState(VK_RIGHT) & 0x8000f)
			g.viewCamera.yaw(-0.01);
	}
	else if (g.select == 1)//controlling the plant object
	{
		if (::GetAsyncKeyState('W') & 0x8000f)
			g.mi1.TranslateRel(0, 0.01, 0);
		if (::GetAsyncKeyState('S') & 0x8000f)
			g.mi1.TranslateRel(0, -0.01, 0);
		if (::GetAsyncKeyState('A') & 0x8000f)
			g.mi1.TranslateRel(0.01, 0, 0);
		if (::GetAsyncKeyState('D') & 0x8000f)
			g.mi1.TranslateRel(-0.01, 0, 0);
	}
	else if (g.select == 2)//controlling the fan object
	{
		if (::GetAsyncKeyState('W') & 0x8000f)
			g.mi2.TranslateRel(0, 0.01, 0);
		if (::GetAsyncKeyState('S') & 0x8000f)
			g.mi2.TranslateRel(0, -0.01, 0);
		if (::GetAsyncKeyState('A') & 0x8000f)
			g.mi2.TranslateRel(0.01, 0, 0);
		if (::GetAsyncKeyState('D') & 0x8000f)
			g.mi2.TranslateRel(-0.01, 0, 0);
	}

	if (::GetAsyncKeyState('0') & 0x8000f)
		g.select = 0;
	else if (::GetAsyncKeyState('1') & 0x8000f)
		g.select = 1;
	else if (::GetAsyncKeyState('2') & 0x8000f)
		g.select = 2;

	// Update the view matrix representing the cameras
	// new position/orientation.
	g.viewCamera.getViewMatrix(&g.view);
	g.g_pDevice->SetTransform(D3DTS_VIEW, &g.view);


	//render the frame
	Render_Frame(hwnd);
	QueryPerformanceCounter(&g.endTime);
	QueryPerformanceFrequency(&g.freq);
	g.timeDelta = (g.endTime.QuadPart - g.startTime.QuadPart) / g.freq.QuadPart;


	//if escape, do that
	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	return S_OK;
}


//release everything created to properly shut down
int GameShutdown()
{
	//releases the resources. First display adapter, then the COM object
	//this is because the COM object created the display adapter
	if (g.g_pDevice)
		g.g_pDevice->Release(); //POSSIBLY C, MAY NEED TO BE CHANGED 

	if (g.g_pD3D)
		g.g_pD3D->Release();

	return S_OK;
}