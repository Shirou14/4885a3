//#pragma once
#ifndef XMESHHEADER_H
#define XMESHHEADER_H

//#include <stdfax.h>
#include "Header.h"
#include "XMesh.h" 

class XMeshInstance : public DXWorldTransform
{
	public:
	XMeshInstance();
	~XMeshInstance() { Release(); }

	void Release();
	void SetMesh(XMesh* pMesh);
	void Render(LPDIRECT3DDEVICE9 pevice);

	private:
	XMesh* m_pMesh;
};

#endif // !XMESHHEADER_H
