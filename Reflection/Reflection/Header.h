#pragma once
#ifndef HEADER_H
#define HEADER_H

#include <d3d9.h>
#include <d3dx9.h>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800

/////////////////////////////////////
// Method Declarations            //
///////////////////////////////////

long CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void CleanD3D(void);
void SetError(char*, ...);
int Render();
int Render_Frame(HWND);
int LoadBitmapToSurface(char*, LPDIRECT3DSURFACE9*, LPDIRECT3DDEVICE9);
void SimpleBitmapDraw(char*, LPDIRECT3DSURFACE9, int, int);
int GameInit(HWND);
int GameLoop(HWND);
int GameShutdown();

#endif // !HEADER_H
